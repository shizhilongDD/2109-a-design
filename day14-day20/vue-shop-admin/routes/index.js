const router = require("koa-router")();
const Mock = require("mockjs");
const { list, titles } = Mock.mock({
  "list|500": [
    {
      "uid|1-10": 1,
      id: "@id",
      "images|5-15": ["https://robohash.org/@id?set=set@integer(1,5)"],
      title: "@ctitle",
      "descs|2-5": ["@cword(10, 20)"],
      "price|1000-10000": 1,
      "oldPrice|10000-20000": 1,
      sku: [
        {
          label: "颜色",
          name: "color",
          list: [
            {
              name: "红色",
            },
            {
              name: "白色",
            },
            {
              name: "黑色",
            },
            {
              name: "天空蓝",
            },
          ],
        },
        {
          label: "版本",
          name: "version",
          list: [
            {
              name: "12G + 256Gb",
            },
            {
              name: "12G + 512Gb",
            },
            {
              name: "16G + 256Gb",
            },
            {
              name: "16G + 1Tb",
            },
          ],
        },
        {
          label: "意外保护",
          name: "protect",
          list: [
            {
              name: "碎屏保 2年     265元",
            },
            {
              name: "意外保 2年     299元",
            },
            {
              name: "碎屏保 1年     179元",
            },
          ],
        },
      ],
      "commend|5-10": [
        {
          id: "@id",
          avator: "https://robohash.org/@id?set=set5",
          name: "@cname",
          content: "@cword(10, 100)",
          "zhuanfa|0-10": 1,
          "pinglun|0-10": 1,
          "dianzan|0-10": 1,
          datetime: "@datetime",
        },
      ],
    },
  ],
  "titles|10": [{
    "id|+1": 1,
    "title": "@ctitle(2)"
  }],
});

router.post('/write/message', ctx => {
  const { id, html } = ctx.request.body;
  const item = list.find(v => v.id === id);
  if(item) {
    item.commend.unshift({
      id: Mock.Random.id(),
      avator: "https://robohash.org/1231231?set=set5",
      name: "匿名用户",
      content: html,
      "zhuanfa": 0,
      "pinglun": 0,
      "dianzan": 0,
      datetime: new Date().toLocaleString(),
    })
    ctx.body = {
      code: 200,
      message: '添加评论成功'
    }
  } else {
    ctx.body = {
      code: 400,
      message: '参数id未找到'
    }
  }
})
router.get("/titles", (ctx) => {
  ctx.body = {
    code: 200,
    data: titles,
    message: "ok",
  };
});
router.get('/list/classify/:uid', ctx => {
  const { uid } = ctx.params;
  const result = list.filter(v => +v.uid === +uid)
  ctx.body = {
    code: 200,
    data: result,
    message: 'ok'
  }
})
router.get("/list", (ctx) => {
  ctx.body = {
    code: 200,
    data: list,
    message: "ok",
  };
});

router.get('/list/:id', ctx => {
  const { id } = ctx.params;
  const item = list.find(v => v.id === id);
  ctx.body = {
    code: 200,
    data: item
  }
})

router.get("/banners", async (ctx, next) => {
  ctx.body = {
    code: 200,
    data: [
      "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/4c2646d6a161a35dfd7858f7fb08df78.jpg?f=webp&w=1080&h=540&bg=372D23",
      "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/f48055c3fc0945acea4ed8953ea61176.jpg?f=webp&w=1080&h=540&bg=FBF1E7",
      "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/f836a43d7fd4892edba484bd0ea4028d.jpg?f=webp&w=1080&h=540&bg=F2EEEB",
      "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/fef746035dc41f84ac858f8ff69d1c45.jpg?f=webp&w=1080&h=540&bg=D0D0D",
    ],
  };
});
router.get("/china/map", (ctx) => {
  ctx.body = {
    code: 200,
    data: [],
  };
});

module.exports = router;
