import { createRouter, createWebHistory } from 'vue-router'

export const layoutChildren = [
  {
    path: '/',
    redirect: '/home'
  },
  {
    path: '/home',
    component: () => import('@/views/home/index.vue'),
    meta: {
      title: '首页',
      icon: 'home-o'
    }
  },
  {
    path: '/classify',
    component: () => import('@/views/classify/index.vue'),
    meta: {
      title: '分类',
      icon: 'apps-o'
    }
  },
  {
    path: '/cart',
    component: () => import('@/views/cart/index.vue'),
    meta: {
      title: '购物车',
      icon: 'cart-o'
    }
  },
  {
    path: '/my',
    component: () => import('@/views/my/index.vue'),
    meta: {
      title: '我的',
      icon: 'user-o'
    }
  }
]

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/login',
      component: () => import('@/views/login/index.vue')
    },
    {
      path: '/',
      component: () => import('@/views/layout/index.vue'),
      children: layoutChildren
    },
    {
      path: '/detail/:id', // params
      component: () => import('@/views/detail/index.vue')
    },
    {
      path: '/more/commend/:id',
      component: () => import('@/views/moreCommend/index.vue')
    },
    {
      path: '/write/message', // query
      component: () => import('@/views/writeMessage/index1.vue')
    }
  ]
})

export default router
