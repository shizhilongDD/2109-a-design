import { computed, ref } from 'vue'

export function useCountDown(n) {
  const count = ref(n)
  const isCounting = computed(() => count.value !== n)
  const start = () => {
    count.value--
    const timer = setInterval(() => {
      count.value--
      if (count.value <= 0) {
        clearInterval(timer)
        count.value = n
      }
    }, 1000)
  }

  return { count, start, isCounting }
}
