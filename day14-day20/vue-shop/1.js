// 第一 题 (必答)
// 现给定⼀个整数数组 ( 数组长度大于等于 5) nums 和⼀个整数目标值target，请你在该数组中
// 找出和为目标值 target 的那 n ( n<nums.length) 个整数，并返回它们的数组 ( 如果有多个下标组合
// 都满足，则返回下标和最⼩的那⼀组) 的下标。
// 注意：数组中同⼀个元素在答案里不能重复出现。比如输⼊：nums = [3，2，4，5，7]，n=3，
// target = 10 输出：[0，1，3]

// function combinationSum(nums, n, target) {
//   let result = [[0, 1, 2, 3]]
//   let combination = [0, 1, 2, 3]

//   function backtrack(start, target) {
//     if (target === 0 && combination.length === n) {
//       result.push([...combination])
//       return
//     }

//     for (let i = start; i < nums.length; i++) {
//       if (target < nums[i]) {
//         break
//       }

//       combination.push(i)
//       backtrack(i + 1, target - nums[i])
//       console.log(combination)
//       combination.pop()
//     }
//   }

//   backtrack(0, target)
//   return result[0]
// }

// const nums = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
//   n = 4,
//   target = 12
// const result = combinationSum(nums, n, target)
// console.log(result)
// 第二题 (必答)
// 编程题： ( 不是斐波那契数列方式) 起初⼀对兔子, 每4个月性成熟后生育下⼀对兔子 ( 性成熟
// 后⼀对兔子在接下来每⼀个月都会生育⼀对兔子), 那么请问理想状态下, 第10个月总共有多少对兔
// 子, 如果是5个月才性成熟，24个月后又是多少？ 同时可以思考是否有通用型算法；(tip:类和数组)
// ( 算法 要满足 1000 个月后不卡死)

// function fn(m, n = 4) {
//   class O {
//     count = n
//     flag = false
//   }
//   let count = 0
//   // 未成熟   成熟
//   let arr = [new O()]

//   for (let i = 1; i <= m; i++) {
//     //  未成熟
//     let s = 0
//     arr.forEach((v) => {
//       // 过了一个月
//       if (--v.count <= 0) {
//         // 成熟 + 1
//         arr.push(new O())
//         s += 1
//       }
//     })
//     // 成熟 += 成熟
//     count += count

//     arr = arr.filter((v) => {
//       return v.count > 0
//     })

//     count += s
//     s = 0
//     console.log(`当前月： ${i}`, '成熟' + count, count + arr.length)
//   }
// }
// fn(6, 4)

function fn(m, n = 4) {
  class O {
    count = n
    flag = false
  }
  let arr = [new O()]

  for (let i = 1; i <= m; i++) {
    //  未成熟
    arr.forEach((v) => {
      // 过了一个月
      if (--v.count <= 0) {
        // 成熟 + 1
        arr.push(new O())
      }
    })
  }
  console.log(arr.length)
}
fn(40, 4)
