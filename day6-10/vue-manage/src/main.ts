import './assets/main.css'
import 'element-plus/es/components/message/style/css'
import { createApp, h } from 'vue'
import { createPinia } from 'pinia'
import { plugin } from 'echarts-for-vue'
import App from './App.vue'
import router from './router'
import * as echarts from 'echarts'

const app = createApp(App)

app.use(createPinia())
app.use(router)
app.use(plugin, { echarts, h })
app.mount('#app')
