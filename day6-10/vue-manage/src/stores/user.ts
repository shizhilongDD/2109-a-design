import { defineStore } from 'pinia'
import { ref } from 'vue'

export const useUserStore = defineStore('user', () => {
  // setup
  const nickname = ref('')
  const avator = ref('')

  const loginSuccess = (data: any) => {
    nickname.value = data.nickname
    avator.value = data.avator
  }

  const quitLogin = () => {
    nickname.value = ''
    avator.value = ''
  }

  return { nickname, avator, loginSuccess, quitLogin }
})
