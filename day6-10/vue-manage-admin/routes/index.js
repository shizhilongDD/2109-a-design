const router = require("koa-router")();
const Mock = require("mockjs");
let { list } = Mock.mock({
  "list|100": [
    {
      id: "@id",
      shopName: "@ctitle",
      shopAddress: "@county(true)",
      shopDescription: "@cword(10, 20)",
      shopTel:
        /^(?:(?:\+|00)86)?1(?:(?:3[\d])|(?:4[5-79])|(?:5[0-35-9])|(?:6[5-7])|(?:7[0-8])|(?:8[\d])|(?:9[1589]))\d{8}$/,
      "shopScore|1-4.1-2": 1,
      "shopSellCount|100-10000": 1,
      "shopType|1": ["生鲜", "水果", "冷饮", "甜品", "鲜花"],
      shopImage: "@image(200x200, @color, 图片)",
    },
  ],
});

/**
 * @swagger
 * /list:
 *   get:
 *     summary: 获取列表
 *     description: 获取列表
 *     tags:
 *       - 列表管理
 *     parameters:
 *       - name: searchValue
 *         in: query
 *         required: false
 *         description: 模糊搜索字段
 *         type: string
 *       - name: type
 *         in: query
 *         required: false
 *         description: 匹配类型
 *         type: string
 *       - name: minScore
 *         in: query
 *         required: false
 *         description: 查找评分最小区间
 *         type: string
 *       - name: maxScore
 *         in: query
 *         required: false
 *         description: 查找评分最大区间
 *         type: string
 *     responses:
 *       10000:
 *         description: 成功获取
 */
// 获取
router.get("/list", (ctx) => {
  const {
    pageSize = 10,
    pageCode = 1,
    searchValue,
    type,
    minScore,
    maxScore,
  } = ctx.request.query;

  const newList = list.filter((v) => {
    // searchValue,
    if (searchValue) {
      if (!v.shopName.includes(searchValue)) return false;
    }
    // type,
    if (type) {
      if (v.shopType !== type) return false;
    }
    // minScore,
    // maxScore,
    if (minScore && !maxScore) {
      if (v.shopScore < minScore) return false;
    } else if (!minScore && maxScore) {
      if (v.shopScore > maxScore) return false;
    } else if (minScore && maxScore) {
      if (v.shopScore < minScore || v.shopScore > maxScore) return false;
    }

    return true;
  });

  ctx.body = {
    data: {
      totalCount: newList.length,
      data: newList.slice(pageSize * (pageCode - 1), pageSize * pageCode),
    },
    message: "ok",
    code: 10000,
  };
});


/**
 * @swagger
 * /list:
 *   delete:
 *     summary: 删除列表项目
 *     description: 删除列表项目
 *     tags:
 *       - 列表管理
 *     parameters:
 *       - name: id
 *         in: query
 *         required: false
 *         description: 删除项目id， 多个id使用逗号隔开
 *         type: string
 *     responses:
 *       10000:
 *         description: 成功获取
 */
// 删除 一个  多个
router.delete("/list", (ctx) => {
  // 接受 删除的id
  const { id } = ctx.request.query;
  // 将id 转化为 数组
  const deleteArrId = id.split(",");
  // 循环筛选数据每一项
  list = list.filter((v) => {
    // 包含的就删除 ， 不包含留下
    return !deleteArrId.includes(v.id);
  });

  ctx.body = {
    code: 10000,
    message: "ok",
  };
});

// 修改
router.post("/list", (ctx) => {
  const body = ctx.request.body;
  const item = list.find((v) => v.id === body.id);
  Object.assign(item, body);

  ctx.body = {
    message: "ok",
    code: 10000,
  };
});

// 添加
router.put("/list", (ctx) => {
  const body = ctx.request.body;
  body.id = Mock.Random.id();
  list.unshift(body);
  ctx.body = {
    code: 10000,
    message: "ok",
  };
});

// 省份店铺数据
router.get('/list/province', ctx => {
  const result = {};
  list.map(v => {
    const province = v.shopAddress.split(' ')[0];

    if(!result[province]) result[province] = 0;

    result[province]++
  })  
  ctx.body = {
    code: 10000,
    data: result,
    message: 'ok'
  }
})



// 垂直领域评分数据
router.get('/list/type', ctx => {
  const result = {
    "生鲜": 0,
    "水果": 0,
    "冷饮": 0,
    "甜品": 0,
    "鲜花": 0
  };

  const result1 = {
    "生鲜": 0,
    "水果": 0,
    "冷饮": 0,
    "甜品": 0,
    "鲜花": 0
  };
  
  list.map(v => {
    result[v.shopType] += v.shopScore
    result1[v.shopType]++
  })  

  const arr1 = [], arr2 = []
  for(let key in result) {
    result[key] = result[key] / result1[key];
    arr1.push(key)
    arr2.push(result[key])
  }

  ctx.body = {
    code: 10000,
    data: {
      arr1, arr2
    },
    message: 'ok'
  }
})

module.exports = router;
