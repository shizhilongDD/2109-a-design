export const options1 = {
  // 提示
  tooltip: {
    show: true
  },
  // 位置
  grid: {
    left: 40,
    top: 5,
    right: 10,
    bottom: 30
    // show: true
  },
  // x 轴线
  xAxis: {
    type: 'category',
    data: ['商超门店', '教育培训', '房地产', '生活服务', '汽车销售', '旅游酒店', '五金建材'],
    axisLine: {
      show: false
    },
    // 刻度线
    axisTick: {
      show: true,
      lineStyle: {
        color: 'rgba(255,255,255,.1)'
      }
    },
    // 刻度标签
    axisLabel: {
      show: true,
      color: 'rgba(255,255,255,.6)'
    }
  },
  //  y 轴线
  yAxis: {
    // show: true,
    type: 'value',
    // 轴线
    axisLine: {
      show: true,
      lineStyle: {
        color: 'rgba(255,255,255,.1)'
      }
    },
    // 刻度线
    axisTick: {
      show: false,
      lineStyle: {
        color: 'red'
      }
    },
    // 分割线
    splitLine: {
      show: true,
      lineStyle: {
        color: 'rgba(255,255,255,.1)'
      }
    },
    // 刻度标签
    axisLabel: {
      show: true,
      color: 'rgba(255,255,255,.6)'
    }
  },
  // 数据
  series: [
    {
      data: [200, 300, 300, 900, 1500, 1200, 600],
      type: 'bar',
      // 柱子背景
      backgroundStyle: {
        // color: 'rgb(74,132,196)'
        // color: '#4786c2'
        color: '#2f89cf',
        opacity: 1
      },
      // 柱子 宽度
      barWidth: 20,
      // 柱子样式
      itemStyle: {
        borderRadius: 6
      }
    }
  ]
}
