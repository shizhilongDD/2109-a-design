import * as echarts from 'echarts'
export const options2 = {
  // backgroundColor: '#0c2d55',
  // 鼠标滑过， 添加提示
  tooltip: {
    trigger: 'axis',
    axisPointer: {
      lineStyle: {
        color: 'rgb(126,199,255)'
      }
    }
  },
  // 统一线段颜色
  color: ['#0184d5', '#00d887'],
  // 图例
  legend: [
    {
      data: [
        {
          name: '安卓'
          // icon: 'rect'
        }
      ],
      // itemWidth: 30,
      // itemHeight: 6,
      top: '0%',
      left: '35%',
      textStyle: {
        color: 'rgba(255,255,255,.6)',
        // fontWeight: "normal",
        fontSize: 16
      }
    },
    {
      data: [
        {
          name: 'IOS'
          // icon: 'rect'
        }
      ],
      // itemWidth: 30,
      // itemHeight: 6,
      top: '0%',
      left: '50%',
      textStyle: {
        color: 'rgba(255,255,255,.6)',
        // fontWeight: "normal",
        fontSize: 16
      }
    }
  ],
  // 位置
  grid: {
    top: '15%',
    left: '2%',
    right: '2%',
    bottom: '10%',
    containLabel: true
  },
  // x 轴线
  xAxis: [
    {
      type: 'category',
      // 控制前后留白
      boundaryGap: false,
      axisLine: {
        //坐标轴轴线相关设置。数学上的x轴
        show: false
      },
      axisLabel: {
        textStyle: {
          color: 'rgba(255,255,255,.6)',
          fontSize: 12
        }
        // alignMinLabel: 'right'
      },
      axisTick: {
        show: true
      },
      data: [
        '01',
        '02',
        '03',
        '04',
        '05',
        '06',
        '07',
        '08',
        '09',
        '11',
        '12',
        '13',
        '14',
        '15',
        '16',
        '17',
        '18',
        '19',
        '20',
        '21',
        '22',
        '23',
        '24'
      ]
    }
  ],
  yAxis: {
    textStyle: {
      color: '#fff',
      fontSize: 24,
      padding: [0, 0, 0, 0]
    },
    // minInterval: 1,
    type: 'value',
    splitLine: {
      show: true,
      lineStyle: {
        color: '#1160a0',
        type: 'dashed'
      }
    },
    axisLine: {
      show: true,
      lineStyle: {
        color: '#008de7'
      }
    },
    axisLabel: {
      show: true,
      textStyle: {
        color: 'rgba(255,255,255,.6)',
        fontSize: 12
      }
    },
    axisTick: {
      show: false
    }
  },
  series: [
    {
      name: '安卓',
      type: 'line',
      symbol: 'none', // 默认是空心圆（中间是白色的），改成实心圆
      // ?????
      smooth: true,
      lineStyle: {
        normal: {
          color: '#0184d5',
          width: 2
        }
      },
      areaStyle: {
        normal: {
          //线性渐变，前4个参数分别是x0,y0,x2,y2(范围0~1);相当于图形包围盒中的百分比。如果最后一个参数是‘true’，则该四个值是绝对像素位置。
          color: new echarts.graphic.LinearGradient(
            0,
            0,
            0,
            1,
            [
              {
                offset: 0,
                color: '#ff702230'
              },
              {
                offset: 0.6,
                color: '#ff702220'
              },
              {
                offset: 1,
                color: '#ff702210'
              }
            ],
            false
          )
        }
      },
      data: [3, 4, 3, 4, 3, 4, 3, 6, 2, 4, 2, 4, 3, 4, 3, 4, 3, 4, 3, 6, 2, 4, 2, 4]
    },
    {
      name: 'IOS',
      type: 'line',
      symbol: 'none', // 默认是空心圆（中间是白色的），改成实心圆
      smooth: true,
      lineStyle: {
        normal: {
          color: '#00d887',
          width: 2
        }
      },
      areaStyle: {
        normal: {
          //线性渐变，前4个参数分别是x0,y0,x2,y2(范围0~1);相当于图形包围盒中的百分比。如果最后一个参数是‘true’，则该四个值是绝对像素位置。
          color: new echarts.graphic.LinearGradient(
            0,
            0,
            0,
            1,
            [
              {
                offset: 0,
                color: '#fff58a30'
              },
              {
                offset: 0.6,
                color: '#fff58a20'
              },
              {
                offset: 1,
                color: '#fff58a10'
              }
            ],
            false
          )
        }
      },
      data: [5, 3, 5, 6, 1, 5, 3, 5, 6, 4, 6, 4, 8, 3, 5, 6, 1, 5, 3, 7, 2, 5, 1, 4]
    }
  ]
}
