export const options3_1 = {
  title: {
    text: '年龄分布',
    textStyle: {
      color: '#fff',
      fontSize: 20
    },
    left: 'center'
  },
  // 提示
  tooltip: {
    trigger: 'item'
  },
  // 位置
  grid: {
    top: '10%',
    left: 0,
    right: 0,
    bottom: 0
  },
  // 图例
  legend: {
    bottom: '5%',
    left: 'center',
    textStyle: {
      color: 'rgba(255,255,255, .6)'
    }
  },
  series: [
    {
      name: '年龄分布',
      type: 'pie',
      // 圆心
      center: ['50%', '40%'],
      radius: ['40%', '60%'],
      color: [
        '#065aab',
        '#066eab',
        '#0682ab',
        '#0696ab',
        '#06a0ab',
        '#06b4ab',
        '#06c8ab',
        '#06dcab',
        '#06f0ab'
      ],
      label: { show: false },
      labelLine: { show: false },
      data: [
        { value: 1, name: '0岁以下' },
        { value: 4, name: '20-29岁' },
        { value: 2, name: '30-39岁' },
        { value: 2, name: '40-49岁' },
        { value: 1, name: '50岁以上' }
      ]
    }
  ]
}

export const options3_2 = {
  title: {
    text: '职业分布',
    textStyle: {
      color: '#fff',
      fontSize: 20
    },
    left: 'center'
  },
  // 提示
  tooltip: {
    trigger: 'item'
  },
  // 位置
  grid: {
    top: '10%',
    left: 0,
    right: 0,
    bottom: 0
  },
  // 图例
  legend: {
    bottom: '5%',
    left: 'center',
    textStyle: {
      color: 'rgba(255,255,255, .6)'
    }
  },
  series: [
    {
      name: '年龄分布',
      type: 'pie',
      center: ['50%', '42%'],
      radius: ['40%', '60%'],
      color: [
        '#065aab',
        '#066eab',
        '#0682ab',
        '#0696ab',
        '#06a0ab',
        '#06b4ab',
        '#06c8ab',
        '#06dcab',
        '#06f0ab'
      ],
      label: { show: false },
      labelLine: { show: false },
      data: [
        { value: 5, name: '电子商务' },
        { value: 1, name: '教育' },
        { value: 6, name: 'IT/互联网' },
        { value: 2, name: '金融' },
        { value: 1, name: '学生' },
        { value: 1, name: '其他' }
      ]
    }
  ]
}

export const options3_3 = {
  title: {
    text: '兴趣分布',
    textStyle: {
      color: '#fff',
      fontSize: 20
    },
    left: 'center'
  },
  // 提示
  tooltip: {
    trigger: 'item'
  },
  // 位置
  grid: {
    top: '10%',
    left: 0,
    right: 0,
    bottom: 0
  },
  // 图例
  legend: {
    bottom: '5%',
    left: 'center',
    textStyle: {
      color: 'rgba(255,255,255, .6)'
    }
  },
  series: [
    {
      name: '兴趣分布',
      type: 'pie',
      center: ['50%', '42%'],
      radius: ['40%', '60%'],
      color: [
        '#065aab',
        '#066eab',
        '#0682ab',
        '#0696ab',
        '#06a0ab',
        '#06b4ab',
        '#06c8ab',
        '#06dcab',
        '#06f0ab'
      ],
      label: { show: false },
      labelLine: { show: false },
      data: [
        { value: 2, name: '汽车' },
        { value: 3, name: '旅游' },
        { value: 1, name: '财经' },
        { value: 4, name: '教育' },
        { value: 8, name: '软件' },
        { value: 1, name: '其他' }
      ]
    }
  ]
}
