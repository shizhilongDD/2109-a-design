// This example requires ECharts v5.5.0 or later
export const options4 = {
  color: ['#0f63d6', '#0f78d6', '#0f8cd6', '#0fa0d6', '#0fb4d6'],
  tooltip: {
    trigger: 'item'
  },
  legend: {
    bottom: '5%',
    left: 'center',
    data: ['浙江', '上海', '广东', '北京', '深圳'],
    textStyle: {
      color: 'rgba(255,255,255,.6)'
    }
  },
  series: [
    {
      clockwise: false,
      name: '浙江',
      type: 'pie',
      radius: ['20%', '30%'],
      center: ['50%', '40%'],
      // adjust the start and end angle
      startAngle: 90,
      endAngle: 450,
      label: { show: false },
      data: [
        { value: 50, name: '1' },
        { value: 50, name: 'qita', itemStyle: { color: 'rgba(0,0,0,.1)' } }
      ]
    },

    {
      clockwise: false,
      name: '上海',
      type: 'pie',
      radius: ['30%', '40%'],
      center: ['50%', '40%'],
      // adjust the start and end angle
      startAngle: 90,
      endAngle: 450,
      label: { show: false },
      data: [
        { value: 60, name: '2' },
        { value: 40, name: 'qita', itemStyle: { color: 'rgba(0,0,0,.1)' } }
      ]
    },
    {
      clockwise: false,
      name: '广东',
      type: 'pie',
      radius: ['40%', '50%'],
      center: ['50%', '40%'],
      // adjust the start and end angle
      startAngle: 90,
      endAngle: 450,
      label: { show: false },
      data: [
        { value: 65, name: '3' },
        { value: 35, name: 'qita', itemStyle: { color: 'rgba(0,0,0,.1)' } }
      ]
    },
    {
      clockwise: false,
      name: '北京',
      type: 'pie',
      radius: ['50%', '60%'],
      center: ['50%', '40%'],
      // adjust the start and end angle
      startAngle: 90,
      endAngle: 450,
      label: { show: false },
      data: [
        { value: 70, name: '4' },
        { value: 30, name: 'qita', itemStyle: { color: 'rgba(0,0,0,.1)' } }
      ]
    },
    {
      clockwise: false,
      name: '深圳',
      type: 'pie',
      radius: ['60%', '70%'],
      center: ['50%', '40%'],
      // adjust the start and end angle
      startAngle: 90,
      endAngle: 450,
      label: { show: false },
      data: [
        { value: 80, name: '5' },
        { value: 20, name: 'qita', itemStyle: { color: 'rgba(0,0,0,.1)' } }
      ]
    }
  ]
}
