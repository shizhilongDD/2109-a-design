# 专高


# 要求
- 晨读 7:30-8:00   面试题 Vue  20道题， 三天时间抄写一部分， 五一之后检查所有。 月考完毕面试
- 违纪 0 
- 作业， 当日当天完成。
- 考试， 认真对待。理论 >= 90分


# 课程安排
- ES6 知识点
- Vue 后端管理系统（双向绑定实现）
- 可视化效果（大屏）
- 聊天（websockt）


# 面试题
- var let const 的联系和区别？
- new运算符做了什么事情
- 常用的数组方法有哪些,返回值是什么
- 介绍下闭包
- vue中watch和computed的区别
- v-if和v-show的区别
- mixin在组件中使用的覆盖逻辑
- vue中父子组件的生命周期执行顺序
- vue父子组件传参的方式
- hash路由和history路由的区别
- vue中key值的作用
- vue路由传参params和query区别？
- 在浏览器地址栏输入url，按下回车之后会经历以下哪些流程
- vue中data的属性可以和methods中的方法同名吗？为什么？
- 如何实现浅拷贝与深拷贝
- cookie,sessionStorage和localStorage的区别
- 为什么vue内的data必须是一个函数
- Event loop
- call,apply,bind 的区别
- vue中NextTick 是做什么的？ 如何实现？
- 在js里0.1+0.2为什么不等于0? 有什么解决方案？