const Mock = require("mockjs");
const fs = require("fs");
const data = Mock.mock({
  "list|2": [
    {
      "title|+1": ["男生", "女生"],
      "list|10": [
        {
          "title|+1": [
            "古代言情",
            "仙剑奇缘",
            "校园言情",
            "浪漫清楚",
            "恐怖灵异",
            "玄幻",
            "都是爱情",
            "乡村",
            "悬疑",
          ],
          "list|4-10": ["@ctitle(4)"],
        },
      ],
    },
  ],
});

fs.writeFileSync('data.json', JSON.stringify(data))