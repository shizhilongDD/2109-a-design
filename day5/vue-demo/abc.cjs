const Mock = require('mockjs')

module.exports = () => {
  return Mock.mock({
    'list|2': [
      {
        'title|+1': ['男生', '女生']
      }
    ]
  })
}
