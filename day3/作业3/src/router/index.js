import { createRouter, createWebHistory } from 'vue-router'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      component: () => import('@/views/callname/index.vue')
    },
    {
      path: '/detail/:id',
      component: () => import('@/views/detail/index.vue')
    },
    {
      path: '/callname',
      component: () => import('@/views/callname/index.vue')
    }
  ]
})

export default router
