const router = require('koa-router')()
const Mock = require('mockjs');
const data = Mock.mock({
  "menus|10": [{
    "id": "@id",
    "image": "https://robohash.org/@id?set=set@integer(1,5)",
    "title": "@ctitle(4)"
  }],
  "banners": [
    "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/fef746035dc41f84ac858f8ff69d1c45.jpg?f=webp&w=1080&h=540&bg=D0D0D",
    "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/415fa53a372e12ce1a502efd7f1aa5b0.jpg?f=webp&w=1080&h=540&bg=F5E2D1",
    "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/f836a43d7fd4892edba484bd0ea4028d.jpg?f=webp&w=1080&h=540&bg=F2EEEB",
    "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/3eceb3075a53a5f723852f97b186cbb9.jpg?f=webp&w=1080&h=540&bg=F5F5F5"
  ],
  "lists|10": [{
    "id": "@id",
    "image": "https://robohash.org/@id?set=set@integer(1,5)",
    "title": "@ctitle",
    "desc": "@cword(10)",
    "score|1-4.1-2": 1,
    "count|10-100000": 1
  }],
  
})
router.get('/menus', ctx => {
  ctx.body = {
    code: 200,
    data: data.menus,
    message: 'ok'
  }
})

router.get('/banners', ctx => {
  ctx.body = {
    code: 200,
    data: data.banners,
    message: 'ok'
  }
})

router.get('/lists', ctx => {
  ctx.body = {
    code: 200,
    data: data.lists,
    message: 'ok'
  }
})

router.get('/list/:id', ctx => {
  const { id } = ctx.params;
  const item = data.lists.find(v => v.id === id);
  ctx.body = {
    code: 200,
    data: item,
    message: 'ok'
  }
})
module.exports = router
