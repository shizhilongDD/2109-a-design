// 数组结构
//  默认值
//  左右结构对等， 结构从左向右一次取值。
// const [a, [b, [c, d, [e, f = 123]]]] = [1, [2, [3, 4, [5, 6]]]]

// console.log(e, f)

// 对象
// 左右结构对等
// const name = '1231231213'

// const { name: name123, age, height, like = '吃饭', o: { oo: [,  { ooo }] } } = 
// { name: '张三', age: 18, height: 180, like: '跑步', o: { oo: [1, { ooo: '哈哈哈' }] } }

// console.log(name123, age, height, like, ooo)


// 函数结构
// function fun({ c: { d: d, asd = 123 } }) {
//   console.log(asd)
// }


// fun({ a: 1, b: 2, c: { d: 3, e: [{ f: 4 }], asd: 111 } })



