// var, let, const
// 1.明明已经存在var声明变量， 为什么会出现let const ？
//  var问题： 变量提升， 重复声明， 没有块级作用， 
// 2. var, let, const 区别和联系

// 作用域
// 1. 全局
// 2. 局部
//  函数
//  局部


// 3.什么是预编译？ 流程是什么？


// const btn = document.querySelector('button');

// for (let i = 0; i < 3; i++) {
//   btn.addEventListener('click', () => {
//     console.log(i)
//   })
// }


// 全局： i = 0     绑定事件  i++
//       i = 1     绑定事件  i++
//       i = 2     绑定事件  i++
//       i = 3     结束
  


// 块级： let i = 0  绑定事件   i++
// 块级： i = 1      绑定事件   i++
// 块级： i = 2      绑定事件   i++
// 块级： i = 3      结束


// console.log(globalThis)