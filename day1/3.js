// 字符串的方法
// includes 包含连续的字符，存在 true， 不存在 false
// startsWith 开头包含连续的字符  存在 true， 不存在 false
// endsWith 结尾包含连续的字符   存在 true， 不存在 false
// repeat(n)  重复n遍  n 属于[0, Infinity), 向下取正
// padStart(len, str) 循环str  头部补位长度为len， 不需要补位不处理
// padEnd(len, str)   循环str  尾部补位长度为len， 不需要补位不处理
// trim trimStart trimEnd 去除空格

// const str = "    1214     " 

// console.log(str.trimStart())
// console.log(str.trimEnd())
